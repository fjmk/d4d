# A docker4drupal template

## Why this template.

[Wodby](https://wodby.com/) created an awesome [docker4drupal](https://github.com/wodby/docker4drupal) development environment with very thorough [documentation](https://wodby.com/docs/stacks/drupal/). I feel there are improvements possible for a quick start for new and existing projects.  
Such as:
- make is a terrible command line tool
- a traefik service for every project
- you have to add the database settings manual for each project
- I added some other 'nice to haves'

### Prerequisites

- The latest version of [docker](https://docs.docker.com/install/) is installed
- The latest version of [docker-compose](https://docs.docker.com/compose/install/) is installed
- The latest version of [ahoy](https://github.com/ahoy-cli/ahoy/releases) (version 2.0.0)
- Existing services on port 80/443 (apache/nginx/MAMP/WAMP) are stopped (and preferably disabled)

One thing you have to do before using the template is starting the central treafik proxy server:
```bash
cd $HOME
curl -L https://gitlab.com/fjmk/d4d/raw/master/traefik.yml -o traefik.yml
docker-compose -f ~/traefik.yml up -d
```
In your browser you can see the web UI of traefik and portainer at http://traefik.docker.localhost/ and http://portainer.docker.localhost/


## Quick setup of a new drupal8 project.

Install a new drupal8 website with composer, add dopker4drupal template and start your development:
```
cd path/to/project_directory
# Install drupal8 with composer
composer create-project drupal-composer/drupal-project:8.x-dev my_site_name_dir --stability dev --no-interaction
# in project directory, add docker4drupal template
cd my_site_name_dir
curl -L https://gitlab.com/fjmk/d4d/raw/master/get-d4d | bash
# start development containers
ahoy up
# install drupal
ahoy drush site-install
```
Now you have your website on http://drupal.docker.localhost/ and the mailhog client on http://mailhog.drupal.docker.localhost/

## Detailed explanation, what is the command get-d4d doing
1. get the default docker4drupal files: .env, docker-compose.yml and  docker-sync.yml
1. get the .ahoy.yml command line tool file
1. configuring settings.php, settings.development.php and development.services.yml
1. add drush directory or add some drush/ files
1. modify .gitignore (we want settings.php in the repo)
1. add tests/behat and tests/backstopjs services

### 1. the default docker4drupal files
This are the latest files from [docker4drupal](https://github.com/wodby/docker4drupal/releases/). I will try to update them once a month.

### 2. The ahoy command line tool
I have chosen this because a lot of drupal developers are using this, see [Our modern development environment at Mass.gov](https://medium.com/massgovdigital/dev-env-5d35b97f3473).  
Installation of ahoy is easy ([see README](http://www.ahoycli.com/en/latest/)):
Macintosh:  
```brew install ahoy --HEAD```  
Linux:  
```sudo curl -L https://github.com/ahoy-cli/ahoy/releases/download/2.0.0/ahoy-bin-linux-amd64 -o /usr/local/bin/ahoy && sudo chmod +x /usr/local/bin/ahoy```  
Windows:  
```curl -L https://github.com/ahoy-cli/ahoy/releases/download/2.0.0/ahoy-bin-windows-amd64  # put in in your $PATH ```  

### 3. configuring settings.php, settings.development.php and development.services.yml
We add some lines to sites/default/settings.php.  
- If sites/default/settings.local.php exists we load the database and site settings from this file. This is the default settings file you have to add manually in your staging and production environments
- If sites/default/settings.development.php exists we load the database and development settings from this file, overriding the database settings in sites/default/settings.local.php. **The get-d4d command is copying the settings.development.php file with the default database settings for the docker4drupal environment**.

The get-d4d command creates the sites/development.services.yml with settings for a development enviroment. You can safely remove this file if you don't want this.

### 4. drush files
- The drush files ```drush/aliases/d4d.drushrc.php``` and ```drush/sites/d4d.site.yml``` makes it possible to use drush aliasses (only the alias ```#d4d.default``` is added). Use these files to add your staging and production sites.
- In ```drush/drush.yml``` is the base_url defined, used in for example the ```ahoy drush uli``` command.

### 5. The behat and backstopjs tests directories

#### behat testing
If you have followed _Quick setup of a new drupal8 project._ and your site is running you can easily run some **behat** tests:
```bash
ahoy behat-init  # you have to run this only once
ahoy behat       # this should give you all green tests
```
TODO: how to use backstop js.
