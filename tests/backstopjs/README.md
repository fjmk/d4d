# Backstop environment

Stolen from: https://www.metaltoad.com/blog/backstopjs-part-deux-javascript-config-and-makefile

## Available environments

- docker (please change base-url at start of project)
- test (please change base-url)
- staging (please change base-url)
- prod (please change base-url)

Environments are set in backstop.js:
```javascript
// if you use one of the options below to pass in, e.g. --env=dev
var environments = {
  'docker': 'http://nginx',
  'pname': 'http://'+process.env.PROJECT_NAME+'_nginx',
//  'test': 'https://test-website.nl',
//  'staging': 'https://staging-website.nl',
//  'prod': 'https://www.prod.nl'
};
var default_environment = 'docker';

```

## urls to test

URLs to test are defined in file paths.js


## Usefull aliases:
```
alias backstop='docker run --rm --shm-size=4gb --cap-add=SYS_ADMIN  -v $(pwd):/src backstopjs/backstopjs "$@"'
alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"
# then
chrome backstop_data/prod_html_report/index.html
```


## Reference & test environments:
```
backstop test --configPath=backstop.js --pathFile=paths --env=prod
backstop reference --configPath=backstop.js --pathFile=paths --env=prod
```

## Reference & test different sites
```
backstop reference --configPath=backstop.js --pathFile=paths --env=prod --refHost=http://staging.nl
backstop test --configPath=backstop.js --pathFile=paths --env=staging --testHost=https://www.prod.nl
```

