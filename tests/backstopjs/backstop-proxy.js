/* Quick guide to BackstopJS commands
 
  backstop reference --configPath=backstop.js --pathFile=paths --env=local --refHost=http://site.dev
  backstop test --configPath=backstop.js --pathFile=paths --env=local --testHost=http://site.dev
 
*/
 
var args = require('minimist')(process.argv.slice(2)); // grabs the process arguments
var dotenv = require('dotenv').config(); // handles basic auth
var defaultPaths = ['/']; // default path just checks the homepage as a quick smoke test
var scenarios = []; // The array that'll have the URL paths to check
 
// env argument will capture the environment URL
// if you use one of the options below to pass in, e.g. --env=dev
var environments = {
  'dev': 'http://nginx',
//  'dev': 'http://'+process.env.PROJECT_NAME+'_nginx',
  'test': 'https://test-website.rvo.nl',
  'zandbak': 'http://agnlinternet.prolocation.net',
  'staging': 'https://acc-website.rvo.nl',
  'prod': 'https://www.rvo.nl'
};
var default_environment = 'prod';
 
// Environments that are being compared
if (!args.env) {
  args.env = default_environment;
}
// if you pass in a bogus environment, it’ll still use the default environment
else if (!environments.hasOwnProperty(args.env)) {
  args.env = default_environment;
}
 
// Site for reference screenshots
if (!args.refHost) {
  args.refHost = environments[args.env];
}
 
// Site for test screenshots
if (!args.testHost) {
  args.testHost = environments[args.env];
}
 
// Directories to save screenshots
var saveDirectories = {
  "bitmaps_reference": "./backstop_data/"+args.env+"_reference",
  "bitmaps_test": "./backstop_data/"+args.env+"_test",
  "html_report": "./backstop_data/"+args.env+"_html_report",
  "ci_report": "./backstop_data/"+args.env+"_ci_report",
  "engine_scripts": "backstop_data/engine_scripts"
};
 
// Work out which paths to use: an array from a file, a supplied array, or defaults
// We'll be using the array from paths.js
if (args.pathFile) {
  var pathConfig = require('./'+args.pathFile+'.js'); // use paths.js file
  var paths = pathConfig.array;
} else if (args.paths) {
  pathString = args.paths; // pass in a comma-separated list of paths in terminal
  var paths = pathString.split(',');
} else {
  var paths = defaultPaths; // keep with the default of just the homepage
}
 
// Scenarios are a default part of config for BackstopJS
// Explanations for the sections below are at https://www.npmjs.com/package/backstopjs
for (var k = 0; k < paths.length; k++) {
  scenarios.push (
    {
      "label": paths[k],
      "referenceUrl": args.refHost+paths[k],
      "url": args.testHost+paths[k],
      "hideSelectors": [],
      "removeSelectors": ["#kcm_rvo_mening"],
      "selectors": ["document"], // "document" will snapshot the entire page
      "delay": 1000,
      "misMatchThreshold" : 0.1
    }
  );
}
 
// BackstopJS configuration
module.exports =
{
  "id": "project_"+args.env+"_config",
  "viewports": [
    {
      "name": "desktop",
      "width": 1600,
      "height": 2000
    }
  ],
  "onBeforeScript": "chromy/onBefore.js",
  "onReadyScript": "chromy/onReady.js",
  "scenarios":
    scenarios,
  "paths":
    saveDirectories,
  "engine": "chromy", // alternate can be slimerjs
  "engineOptions": {
    waitTimeout: 60000,
    chromeFlags: [ "--disable-gpu", "--force-device-scale-factor=1", "--disable-infobars=true", "--proxy-server=http://10.20.64.10:8080" ]
  },
  "asyncCaptureLimit": 2,
  "asyncCompareLimit": 20,
  "report": ["browser"],
  "debug": false
};