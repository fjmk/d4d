var pathConfig = {};
 
pathConfig.array = [
  '/', // homepage
  '/user/login',
  '/contact',
  '/search/node?keys=xyz',
  '/search/node/help'
]
 
module.exports = pathConfig;
